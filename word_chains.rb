class TreeNode
  
  attr_accessor :value, :parent, :left, :right, :children
  
  def initialize(value)
    @parent = nil 
    @left = nil
    @right = nil
    @value = value
    @children = []
  end
  
  def set_left(left_child)
    self.left.parent = nil unless self.left == nil
    self.left = left_child
    left_child.parent = self
  end
  
  def set_right(right_child)
    self.right.parent = nil unless self.right == nil
    self.right = right_child
    right_child.parent = self
  end
  
  def set_child(child)
    self.children << child
    child.parent = self
  end
    
  #searches always return a TreeNode object  
  def dfs(target=nil)
    
    #base1
    return self if value == target
    
    children.each do |child|
      next if child.nil?
      result = child.dfs(target)
      return result unless result.nil?
      
    end
    #base2
    nil
  end
  
  def bfs1(search_value = nil, &prc)
    nodes = [self]
    until nodes.empty?
      node = nodes.shift
    
      if block_given?
        return node if prc.call(self)
      else
        return node if node.value == search_value
      end
      
      nodes.concat(node.children)
    end
    
    nil
  end

  def bfs(target = nil, &prc)
    nodes = [self]
    until nodes.empty?
      node = nodes.shift

      if block_given?
        return node if prc.call(self)
      else
        return node if node.value == target
      end

      nodes.concat(node.children)
    end
    
    nil
  end
  
end

def find_chain(start_word, end_word, dictionary1)
  dictionary = File.readlines("dictionary.txt")
  dictionary.map! do |word|
    word.chomp
  end
  dictionary.select! do |word|
    word if word.length == start_word.length
  end
  
  root_node = TreeNode.new(start_word)
  create_tree(root_node, end_word, dictionary)
  puts "#{create_path(root_node,end_word).length}"
end


def create_path(root_node, end_word)
  current_node = root_node.bfs(end_word)
  path = []
  
  until path[-1] == nil
    if current_node.parent.nil?
      path << nil
    else
      path << current_node.parent.value 
    end
    current_node = current_node.parent
  end
  path
end


def create_tree(root_node, end_word, dictionary)
  search_stack = [root_node]
  visited_words = []
  until search_stack.empty?
    current_node = search_stack.shift
    if current_node.value == end_word
      return root_node
    else
      next_words = adjacent_words(current_node.value, dictionary)
      p next_words
      next_words.each do |word|
        if !visited_words.include?(word)
          new_node = TreeNode.new(word)
          
          new_node.parent = current_node
          search_stack << new_node
          visited_words << current_node.value
        end
      end
    end
    search_stack
  end
end

def adjacent_words(word, dictionary)
  #produces an array
  output = []
  dictionary.each do |dictionary_word|
    count = 0
    dictionary_word.split("").each_with_index do |letter,i|
      if letter != word[i]
        count += 1
      end
    end
    output << dictionary_word if count == 1
  end
  output
end

p find_chain("cat","hat","dictionary.txt")

