class Hangman
  
  attr_reader :chooser, :guesser
  
  def initialize(chooser, guesser)
    @chooser = chooser
    @guesser = guesser
    play_game
  end
  
  def play_game
    secret_word_length = self.chooser.pick_word
    #returns a length
    puts "#{secret_word_length} letter#{"s" if secret_word_length > 1}"
    correct_word = Array.new(secret_word_length, "_")
    guesses = 0
    
    until guesses >= 10
      
      puts "What is your guess?"
      
      current_guess        = self.guesser.guess_letter
      updated_correct_word = self.chooser.evaluate(current_guess, correct_word) 
      #output should be an array that updates correct_word
      
      puts "#{updated_correct_word.join("")}"
    
      unless updated_correct_word.include?("_")
        return puts "Congratulations! You win!"
      end
      
      if updated_correct_word == correct_word
        guesses += 1
      end
      correct_word = updated_correct_word
      
    end
    puts "You lose." 
    if self.chooser.is_a?(ComputerPlayer)
      puts "The word is #{chooser.chosen_word}"
    end
  end
  
end


class HumanPlayer
  
  def pick_word
    "Pick a word. How many letters is your word?"
    gets.chomp.to_i
  end
  
  def guess_letter
    gets.chomp
  end
  
  def evaluate(letter, correct_word)
    #return an array 
    positions = gets.chomp.split("")
    
    positions.select! do |position|
      position unless (position == " ") || (position == ",")
    end
    
    positions.map! do |position|
      position.to_i
    end
    
    positions.select! do |position|
      position if position.is_a?(Integer)
    end
    puts "#{correct_word}"
    
    positions.each do |index|
      correct_word[index] = letter
    end
    correct_word
    
  end
  
end


class ComputerPlayer
  attr_reader :chosen_word, :dictionary, :guessed_letters
  
  def initialize
    @dictionary = File.readlines("dictionary.txt")    
    @chosen_word = self.dictionary.sample
    @guessed_letters = ("a".."z").to_a
  end
  
  def pick_word
    self.chosen_word.length
  end
  
  def guess_letter
    guess = self.guessed_letters.sample
    self.guessed_letters.select! do |letter|
      letter unless letter == guess
    end
    guess
  end
  
  def evaluate(letter, correct_word)
    self.chosen_word.split("").each_with_index do |actual_letter, i|
      if letter == actual_letter
        correct_word[i] = letter
      end
    end
    correct_word
  end
  
end


chooser = ComputerPlayer.new
guesser = ComputerPlayer.new
game = Hangman.new(chooser, guesser)








